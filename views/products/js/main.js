let app = new App()
app.bindDelete()
app.deleteUser()
app.bindCollection()


//pagination
var current_page = 1;
var records_per_page = 10;
var j=1;

document.getElementById("btn_next").addEventListener("click", () => {
    changePage(++current_page);
});

document.getElementById("btn_prev").addEventListener("click", () => {
    changePage(--current_page);
});

var objJson


async function changePage(page)
{
    objJson = await app.getAllUsers();
    var btn_next = document.getElementById("btn_next");
    var btn_prev = document.getElementById("btn_prev");
    var listing_table = document.getElementById("usersContent");
    var page_span = document.getElementById("page");
 
    // Validate page
    if (page < 1) page = 1;
    if (page > numPages()) page = numPages();

    listing_table.innerHTML = "";
    
    for (var i = (page-1) * records_per_page; i < (page * records_per_page); i++) {
        
        if(objJson.data[i] != null){
        listing_table.innerHTML += `
                <tr id="tableRow" id="${objJson.data[i].id}">
            <td>
            ${objJson.data[i].name}
            </td>
            <td class="text-center">
              <button class="btn btn-primary me-3 collectionBtn" data-user-id="${objJson.data[i].id}" onclick="app.bindSingleCollection(${objJson.data[i].id})">Get Collection</button>
              <button class="btn btn-danger deleteCollectionBtn" data-bs-toggle="modal" data-bs-target="#deleteUserModal" user-id="${objJson.data[i].id}" user-name="${objJson.data[i].name}" table-row=${j}>Delete User</button>
            </td>
          </tr>
                `
        }
    
    }
    
    page_span.innerHTML = page;

    if (page == 1) {
        btn_prev.style.visibility = "hidden";
    } else {
        btn_prev.style.visibility = "visible";
    }

    if (page === numPages()) {
        btn_next.style.visibility = "hidden";
    } else {
        btn_next.style.visibility = "visible";
    }
}
function numPages()
{
    return Math.ceil(objJson.data.length / records_per_page);
}
window.addEventListener("load",()=>{changePage(current_page)})






