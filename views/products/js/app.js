let cikur

 cikur = document.getElementById("tabel-data")
console.log(document.getElementById("tabelSingleCollection"))
class App{
    constructor(){
        this.collection = document.getElementsByClassName("collectionBtn")
        this.deleteCollection = document.querySelectorAll("deleteCollectionBtn")
        this.submitCreate = document.getElementById("submitCreate")
        this.table = document.getElementById("usersContent")
        this.modalDelete = document.getElementById("deleteUserModal")
        this.submitDelete = document.getElementsByClassName("submitDelete")
        this.baris = cikur
        this.loader = document.querySelector(".loader")
        this.overlay = document.querySelector("#overlay")
    }

    loaders(){
       this.loader.classList.toggle("d-none")
       this.overlay.classList.toggle("d-none")
    }

   async getAllUsers(){
    let listing_table = document.getElementById("usersContent");
        let main = this.table
        let produk = []
        let i = 1
        await fetch("/api/users")
        .then(res => res.json())
        .then(data => {
            let vessel = JSON.stringify(data)
            produk = JSON.parse(vessel)
        })
        return produk
    }

    submitCreateUser(){
        //check if submitCreate is not null
            let name = document.getElementById("name").value
            console.log(name)
            //fetch to create user using post method
            fetch("/api/users",{
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({
                    name: name
                })
            }).then(res => res.json())
            .then(data => {
                console.log(data)

                //create alert
                let main = document.getElementById("main")
                let alert = document.createElement("div")
                alert.className = "alert alert-success"
                alert.innerHTML = data.message
                //append alert to body and put it on top of the page
                
                this.loaders()
                setTimeout(() => {
                    main.prepend(alert)
                    this.loaders()
                    changePage(1)
                }, 1000)
                //remove alert after 3 seconds
                setTimeout(() => {
                    alert.remove()
                }, 3000)
            })
}

bindDelete(){
    //add event listener when modal is shown
    let bn = this.baris
    let deleteModal
    document.addEventListener("show.bs.modal", (e) => {
        //check if the modal is delete modal
        if(e.target.id == "deleteUserModal"){
        deleteModal = document.getElementById("deleteUserModal")
        this.row = e.relatedTarget.parentNode.parentNode.rowIndex
        // Button that triggered the modal
        let button = e.relatedTarget;
        // Extract info from data-bs-* attributes
        let recipient = button.getAttribute("user-id");
        let nama = button.getAttribute("user-name");
        // If necessary, you could initiate an AJAX request here
        // and then do the updating in a callback.
        // Update the modal's content.
        // Create a class attribute:
        const att = document.createAttribute("id");

        // Set the value of the class attribute:
        att.value = recipient;
        // Add the class attribute to the first h1:
        const yes = deleteModal.querySelector(".submitDelete");
        const teks = deleteModal.querySelector("#namaUser");
        yes.setAttributeNode(att);
        teks.innerHTML = `Anda akan menghapus user bernama <strong>${nama}</strong> beserta koleksi bukunya, apakah anda yakin?`;
    }    
    })
}

deleteUser(){
    let baris = this.baris
    let bn = this.row
    setTimeout(() => {
let deleteBtn = document.querySelector(".submitDelete")
deleteBtn.addEventListener("click",(e)=>{
   let submit = e.target
   let id = submit.getAttribute("id")
   fetch(`/api/users/${id}`,{
method: "DELETE"
}).then(res => res.json())
.then(data => {
    //console.log(data)
    //create alert
    let main = document.getElementById("main")
    let alert = document.createElement("div")
    alert.className = "alert alert-success"
    alert.innerHTML = data.message
    //append alert to body and put it on top of the page
    main.prepend(alert);
    this.loaders()
    setTimeout(() => {
        changePage(1)
        this.loaders()
    }, 1000)
    setTimeout(() => {
        alert.remove()
    }, 3000)
})
}
, 1000)
})
}

async bindCollection(){
    let loadedUsers = await this.getAllUsers()
    console.log(loadedUsers)
    let name = document.getElementById("pilihUser")
    console.log(name)
    document.addEventListener("show.bs.modal", (e) => {
        // Button that triggered the modal
        loadedUsers.data.forEach(obj=>{
            name.innerHTML += `<option value="${obj.id}">${obj.name}</option>`
        })
    },{once:true})
}

async getSingleCollection(id){
    let produk = []

    await fetch(`/api/products/${id}`)
    .then(res => res.json())
    .then(data => {
        let vessel = JSON.stringify(data)
        produk = JSON.parse(vessel)
    })
    return produk
}

async bindSingleCollection(id){
    let loadedCollection = await this.getSingleCollection(id)
    console.log(loadedCollection)
    let name = document.getElementById("ambilNamaUser")
    name.innerHTML = `${loadedCollection.message}`
    let tabel = document.getElementById("tabelSingleCollection")
    const myModal = new bootstrap.Modal(document.getElementById('singleCollectionModal'))
    console.log(tabel)
    this.loaders()
    setTimeout(() => {
    this.loaders()
    document.addEventListener("show.bs.modal", (e) => {
        // Button that triggered the modal
      document.getElementById('reseter').addEventListener('click', function() {
        tabel.innerHTML=``})
        loadedCollection.data.forEach(obj=>{
            tabel.innerHTML += `<tr class="table-info" row-data="${obj.id}" id="collection-${obj.id}">
            <td>${obj.product_name}</td>
            <td>${obj.author}</td>
            <td>${obj.publisher}</td>
            <td>${obj.pubyear}</td>
            <td><div class="hstack gap-1" id="aksi">
            <a href="update/${obj.id}/${obj.name}" type="button" aksi-id="${obj.id}" class="btn btn-warning" onclick="app.bindRowCollection(${obj.id})">Edit</a>
            <a href="delete/${obj.id}/${obj.name}" type="button" aksi-id="${obj.id}" class="btn btn-danger">Hapus</a>
            </div>
            </td>
            </tr>
            `
        })
   
    },{once:true})
    myModal.show()
}, 3000)
}

// async bindRowCollection(id){
//     //let loadedCollection = await this.getSingleCollection(id)
//     //console.log(loadedCollection)
//     document.addEventListener("click",(e)=>{
//         let id = e.target.getAttribute("aksi-id")
//         let rowIni = e.target
//         console.log(rowIni)
//         rowIni.parentNode.classList.toggle("d-none")
//         updateRow = rowIni.parentNode.nextElementSibling.classList.toggle("d-none")
//         console.log(referSelf,updateRow)
//         document.querySelector(`#collection-${id}`).classList.toggle("d-none")
//         document.querySelector(`#collections-${id}`).classList.toggle("d-none")
//     })
// }

submitCollection(params = null){
    //check if submitCreate is not null
        let id_user = document.getElementById("pilihUser").value
        let product_name = document.getElementById("product_name").value
        let author = document.getElementById("author").value
        let publisher = document.getElementById("publisher").value
        let pubyear = document.getElementById("pubyear").value
        //button to listen
        //fetch to create user using post method
        fetch("/api/products",{
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                id_user: id_user,
                product_name: product_name,
                author:author,
                publisher:publisher,
                pubyear:pubyear
            })
        }).then(res => res.json())
        .then(data => {
            console.log(data)
            //create alert
            let main = document.getElementById("createCollectionForm")
            let alert = document.createElement("div")
            alert.className = "alert alert-success"
            alert.innerHTML = data.message
            this.loaders()
            if(params == 'go'){
                setTimeout(() => {
                    this.loaders()
                    document.getElementById("main").prepend(alert)
                    main.reset()
                }, 1000)
            }else{
            setTimeout(() => {
                main.prepend(alert)
                main.reset()
                this.loaders()
            }, 1000)
        }
            //remove alert after 3 seconds
            setTimeout(() => {
                alert.remove()
            }, 3000)
        })
}

resetTable(){
    console.log(this.tombol)
    let tombol = this.reseter
    tombol.addEventListener("click", (e) => {
    let tabel = document.getElementById("tabelSingleCollection")
    tabel.innerHTML = ``
    })
}
}