const router = require("express").Router();

const productController = require('../controllers/productController')
const adminProductController = require('../controllers/admin/productController')

const uploader = require('../middleware/uploader')

// API
router.get('/api/products', productController.getProducts)
router.post('/api/products', uploader.single('image'), productController.createProduct)
router.get('/api/products/:id', productController.getProductById)
router.put('/api/products/:id', productController.updateProduct)
router.delete('/api/products/:id', productController.deleteProduct)

// dashboard admin
router.get('/', adminProductController.getProducts)
router.get('/products', adminProductController.createProductPage)
router.post('/products', adminProductController.createProduct)
router.get('/search', adminProductController.searchProducts)


module.exports = router
