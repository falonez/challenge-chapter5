'use strict';

const faker = require('faker')

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

     var products = [];

        for (let i = 0; i < 10; i++) {
            const seedData = {
              name: faker.name.firstName(),
              userId: faker.datatype.number(10, 50),
              size: faker.name.lastName(),
              imageUrl: "https://ik.imagekit.io/tinpet/hp_terbaru.png_Rq7adJd50.png?ik-sdk-version=javascript-1.4.3&updatedAt=1664872082668",
              createdAt: new Date(),
              updatedAt: new Date()
            };
            products.push(seedData);
        }

  await queryInterface.bulkInsert('products', products, {});
},

  async down(queryInterface, Sequelize) {
  /**
   * Add commands to revert seed here.
   *
   * Example:
   * await queryInterface.bulkDelete('People', null, {});
   */
}
};
