const imagekit = require("../lib/imageKit");
const { products } = require("../models");

// get atau retrieve function controller
async function getProducts(req, res) {
    try {
        const responseData = await products.findAll()
        res.status(200).json({
            'test': '123 sayang semuanya',
            'data': responseData
        })
    } catch (err) {
        console.log(err.message)
    }
}

// create new data
async function createProduct(req, res) {
    try {
        // process file naming        
        const split = req.file.originalname.split('.')
        console.log(split)
        const extension = split[split.length - 1]
        console.log(extension)

        const imageName = req.file.originalname + '.' + extension
        console.log(imageName)

        // upload file 
        const img = await imagekit.upload({
            file: req.file.buffer,
            fileName: imageName
        })

        console.log(img)

        const name = req.body.name
        const size = req.body.size
        // const { name, size } = req.body

        const newProduct = await products.create({ 
            name: name,
            imageUrl: img.url,
            size: size,
        })
        
        res.status(200).json({
            'status': 'success',
            'data': newProduct
        })
    } catch (err) {
        res.status(400).json({
            'message': err.message
        })
    }
}

// get atau retrieve function controller
async function getProductById(req, res) {
    try {
        const id = req.params.id
        const product = await products.findByPk(id)
        // console.log(product)
        if (product === null) {
            res.status(404).json({
                'message': `data pada ${id} tersebut tidak ada`
            })
        }

        res.status(200).json({
            'data': product
        })
    } catch (err) {
        console.log(err.message)
    }
}

// update data
async function updateProduct(req, res) {
    try {
        const { name } = req.body
        const id = req.params.id
        const product = await products.update({
            name
        }, {
            where: {
                id
            }
        })

        res.status(200).json({
            'success': true
        })
    } catch (err) {
        console.log(err.message)
    }
}

// delete data
async function deleteProduct(req, res) {
    try {
        const id = req.params.id
        await products.destroy({
            where: {
                id
            }
        })

        res.status(200).json({
            'message': 'success delete produk'
        })
    } catch (err) {
        console.log(err.message)
    }
}


module.exports = {
    getProducts,
    createProduct,
    getProductById,
    updateProduct,
    deleteProduct
}