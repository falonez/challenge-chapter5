const { Op, and } = require("sequelize");
const { products } = require("../../models");

// GET /books
async function getProducts(req, res) {
    const items = await products.findAll()
    res.render("products/index", {
        items,
    });
};

// GET /books/create
async function createProductPage(req, res) {
    res.render("products/create");
}

// untuk kalau menggunakan method post di ejs nya
async function createProduct(req, res) {
    const name = req.body.name
    await products.create({ name: name })
    res.redirect(200, "/");
}

// search page
async function searchProducts(req, res) {
    let items
    // console.log(req.query.size)
    // if (req.query.size) {
    //     items = await products.findAll({
    //         where: {
    //             [Op.or]: [
    //                 { name: req.query.name },
    //                 { size: req.query.size }
    //             ]
    //         }
    //     })    
    // }
    items = await products.findAll({
        where: {
            [Op.and]: [
                { name: req.query.name },
                { size: req.query.size }
            ]
        }
    })
    res.render("products/index", {
        items,
    });
};

module.exports = {
    getProducts,
    createProduct,
    createProductPage,
    searchProducts,
}