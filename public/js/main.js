// Add Data Product in dashboard admin
async function addDataProduct() {
  const data = {
    name: document.getElementById("addName").value,
  };
  if ( data.name == "" ) {
    alert("Data Tidak Boleh Kosong!");
  } else {
    const restAPI = await axios.post("/api/products", data);
    if (restAPI.data.status) {
      alert("Berhasil Tambah Data");
    } else {
      alert("500 Server Error [Tidak Dapat Menambahkan data]");
    }
  }
}
